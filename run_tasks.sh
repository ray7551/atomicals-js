#!/bin/bash

process_count=${1:-1}
tick=${2:-neutron}
max_gas=${3:-150}
echo "process count: $process_count"
echo "tick: $tick"

while true; do
    # 从 API 获取数据
    # 解析 JSON 并获取 halfHourFee
    halfHourFee=$(echo $(curl -s https://mempool.space/api/v1/fees/recommended) | jq '.halfHourFee')
    echo "halfHourFee: $halfHourFee"

    # 确定 gas 的值
    # 检查 halfHourFee 是否为空或非数字
    if [[ -z "$halfHourFee" || ! "$halfHourFee" =~ ^[0-9]+$ ]]; then
	echo "use default gas"
        gas=$max_gas
    elif [ "$halfHourFee" -gt $max_gas ]; then
	echo "gas too high, use default gas"
        gas=$max_gas
    else
        gas=$halfHourFee
    fi
    echo "curretn gas: $gas"

    name=$(echo "$tick-$(date +'%y-%m-%d-%H:%M')")
    # 使用 PM2 启动进程
    pm2 start "yarn cli mint-dft $tick --disablechalk --satsbyte=$gas" -i $process_count --name $name

    sleep 3600

    # 停止所有进程
    pm2 delete $name 
done